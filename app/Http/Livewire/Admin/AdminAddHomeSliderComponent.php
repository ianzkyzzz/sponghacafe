<?php

namespace App\Http\Livewire\Admin;
use App\models\HomeSlider;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;

use Livewire\Component;

class AdminAddHomeSliderComponent extends Component
{
    use WithFileUploads;
    public $title;
    public $subtitle;
    public $price;
    public $link;
    public $image;
    public $status;

    public function mount()
    {
        $this->status = 0;
  

    }
    public function addSlider()
    {
        if(Auth::user()->isAdmin == 0)
        {
            return redirect()->route('admin.orders');
        }
        else
        {
            $slider = new HomeSlider();
            $slider->title = $this->title;
            $slider->subtitle = $this->subtitle;
            $slider->price = $this->price;
            $slider->link = $this->link;
            $imageName = Carbon::now()->timestamp. '.' . $this->image->extension();
            $this->image->storeAs('sliders',$imageName);
            $slider->image = $imageName;
            $slider->status = $this->status;
            $slider->save();
            session()->flash('message', 'New Slide Added Successfully!');
        }
        

    }
    public function render()
    {
        return view('livewire.admin.admin-add-home-slider-component')->layout('layouts.base');
    }
}
