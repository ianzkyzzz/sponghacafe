<?php

namespace App\Http\Livewire\Admin;
use App\models\Menu;
use App\models\Category;
use Livewire\Component;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\WithFileUploads;

class AdminAddMenuComponent extends Component
{
    use WithFileUploads;
    public $name;
    public $slug;
    public $short_description;
    public $description;
    public $regular_price;
    public $sale_price;
    public $SKU;
    public $stock_status;
    public $featured;
    public $quantity;
    public $image;
    public $category_id;
    
    public function mount()
    {
        $this->stock_status = 'instock';
        $this->featured = 0;
    }
    public function generateSlug()
    {
        $this->slug = Str::slug($this->name,'-');
    }
    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'name'=>'required',
            'slug'=>'required|unique:menus',
            'short_description'=>'required',
            'description'=>'required',
            'regular_price'=>'required|numeric',
        
            'stock_status'=>'required',
          
            'image'=>'required|mimes:jpg,jpeg,png',
            'category_id'=>'required|numeric',
        ]);
    }
    public function addMenu()
    {
        if(Auth::user()->isAdmin == 0)
        {
            return redirect()->route('admin.orders');
            
        }
        else
        {
            $this->validate([
                'name'=>'required',
                'slug'=>'required|unique:menus',
                'short_description'=>'required',
                'description'=>'required',
                'regular_price'=>'required|numeric',
             
                'stock_status'=>'required',
             
                'image'=>'required|mimes:jpg,jpeg,png',
                'category_id'=>'required|numeric',
        
                ]);
                $menu = new Menu();
                $menu->name = $this->name;
                $menu->slug = $this->slug;
                $menu->short_description = $this->short_description;
                $menu->description = $this->description;
                $menu->regular_price = $this->regular_price;
                if($this->sale_price)
                {
                    $menu->sale_price = $this->sale_price;
                }
              
             
                $menu->stock_status = $this->stock_status;
                $menu->featured = $this->featured;
              
                $imageName = Carbon::now()->timestamp. '.' . $this->image->extension();
                $this->image->storeAs('products',$imageName);
                $menu->image = $imageName;
                $menu->category_id = $this->category_id;
                $menu->save();
                session()->flash('message', 'Menu Added Succesfully!');
        }
       

    }

    public function render()
    {
        $categories = Category::all();
        return view('livewire.admin.admin-add-menu-component',['categories'=>$categories])->layout('layouts.base');
    }
}
