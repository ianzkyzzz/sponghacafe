<?php

namespace App\Http\Livewire\Admin;
use App\models\Category;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Livewire\WithPagination;

class AdminCategoryComponent extends Component
{
    use WithPagination;

    public function deleteCategory($id)
    {
        if(Auth::user()->isAdmin == 0)
        {
            return redirect()->route('admin.orders');
            
        }
        else
        {
            $category = Category::find($id);
            $category->delete();
            session()->flash('message', 'Category Deleted Successfully');
        }
   
    }
    public function render()
    {
        $categories = Category::paginate(5);
        return view('livewire.admin.admin-category-component',['categories'=>$categories])->layout('layouts.base');
    }
}
