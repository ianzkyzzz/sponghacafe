<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class AdminDahboardComponent extends Component
{
    public function render()
    {
        return view('livewire.admin.admin-dahboard-component')->layout("layouts.base");
    }
}
