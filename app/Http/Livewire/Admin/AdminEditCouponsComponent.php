<?php

namespace App\Http\Livewire\Admin;
use App\models\Coupon;
use Livewire\Component;

class AdminEditCouponsComponent extends Component
{
    public $code;
    public $type;
    public $value;
    public $cart_value;
    public $coupon_id;
    public function mount($coupon_id)
    {
        $this->coupon_id = $coupon_id;
        $coupon = Coupon::where('id', $coupon_id)->first();
        $this->coupon_id = $coupon->id;
        $this->code = $coupon->code;
        $this->type = $coupon->type;
        $this->value = $coupon->value;
        $this->cart_value = $coupon->cart_value;
        
    }
    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'code' => 'required|unique:coupons',
            'type' =>'required',
            'value' => 'required|numeric',
            'cart_value' => 'required'
        ]);
    }
    public function updateCoupon()
    {
        $this->validate([
            'code' => 'required|unique:coupons',
            'type' =>'required',
            'value' => 'required|numeric',
            'cart_value' => 'required'
        ]);
        $coupon = Coupon::find($this->coupon_id);
        $coupon->code = $this->code;
        $coupon->type = $this->type;
        $coupon->value = $this->value;
        $coupon->cart_value = $this->cart_value;
        $coupon->save();
        session()->flash('message','Coupon Edited Successfully!');
    }
    public function render()
    {
        return view('livewire.admin.admin-edit-coupons-component')->layout('layouts.base');
    }
}
