<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\models\Menu;
use App\models\Category;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class AdminEditMenuComponent extends Component
{
    use WithFileUploads;
    public $name;
    public $slug;
    public $short_description;
    public $description;
    public $regular_price;
    public $sale_price;

    public $stock_status;
    public $featured;

    public $image;
    public $category_id;
    public $newimage;
    public $menu_id;

    public function mount($menu_slug)
    {
        $menu = Menu::where('slug',$menu_slug)->first();
    $this->name = $menu->name;
    $this->slug = $menu->slug;
    $this->short_description = $menu->short_description;
    $this->description = $menu->description;
    $this->regular_price = $menu->regular_price;
    $this->sale_price = $menu->sale_price;

    $this->stock_status = $menu->stock_status;
    $this->featured = $menu->featured;
  
    $this->image = $menu->image;
    $this->category_id = $menu->category_id;
    $this->menu_id = $menu->id;
    }
 
    public function generateSlug()
    {
        $this->slug = Str::slug($this->name,'-');
    }
    public function updateMenu()
    {
        
  
        $menu = Menu::find($this->menu_id);
        $menu->name = $this->name;
        $menu->slug = $this->slug;
        $menu->short_description = $this->short_description;
        $menu->description = $this->description;
        $menu->regular_price = $this->regular_price;
        $menu->sale_price = $this->sale_price;
   
        $menu->stock_status = $this->stock_status;
        $menu->featured = $this->featured;
    
        if($this->newimage)
        {
            $imageName = Carbon::now()->timestamp. '.' . $this->newimage->extension();
            $this->newimage->storeAs('products',$imageName);
            $menu->image = $imageName;
        }
       
        $menu->category_id = $this->category_id;
        $menu->save();
        session()->flash('message', 'Menu Edited Succesfully!');
    }
    public function render()
    {
        $categories = Category::all();
        return view('livewire.admin.admin-edit-menu-component',['categories'=>$categories])->layout('layouts.base');
    }
}
