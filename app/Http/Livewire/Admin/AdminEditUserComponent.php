<?php

namespace App\Http\Livewire\Admin;
use App\models\User;
use Livewire\Component;

class AdminEditUserComponent extends Component
{
    public $user_id;
    public $name;
    public $email;
    public $access;



    public function mount($user_id)
    {
        $this->user_id = $user_id;
        $user = User::where('id',$user_id)->first();
        $this->user_id = $user->id;
        $this->name = $user->name;
        $this->email = $user->email;
    }
    public function updateUser()
    {
    
        $user = User::find($this->user_id);
        if($this->access=='ADM')
        {
            $user->uytpe ='ADM';
            $user->isAdmin = 1;
        }
        else if($this->access=='CAS')
        {
            $user->uytpe ='ADM';
            $user->isAdmin = 0;
        }
        else{
            $user->uytpe ='USR';
            $user->isAdmin = 0;
        }

        $user->save();
        session()->flash('message','Category Edited Successfully!');
    }
    public function render()
    {
        return view('livewire.admin.admin-edit-user-component');
    }
}
