<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\models\Menu;
use App\models\Category;
use Livewire\WithPagination;

class AdminMenuComponent extends Component
{
    use WithPagination;
    public function deleteMenu($id)
    {
        $menu = Menu::find($id);
        $menu->delete();
        session()->flash('message', 'Menu Deleted Successfully');
    }
    public function render()
    {
        $menus = Menu::paginate(10);
        return view('livewire.admin.admin-menu-component',['menus'=>$menus])->layout('layouts.base');
    }
}
