<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\models\Order;
use App\models\User;
use App\models\table;
use Livewire\WithPagination;
class AdminOrderComponent extends Component
{
    use WithPagination;
    public function finishOrder($id,$tblid)
    {
        $order = Order::find($id);
        $order->status = 'done';
        $order->save();
        session()->flash('message', 'Order Finished');
        $this->vacateTable($tblid);
    }
    private function vacateTable($tblid)
    {
        if($tblid !='99')
        {
            $tbl = table::find($tblid);
            $tbl->status = '0';
            $tbl->save();
        }
      
       
    }
    public function cancelOrder($id)
    {
        $order = Order::find($id);
        $order->status = 'cancelled';
        $order->save();
        session()->flash('message_cancel', 'Order Cancelled');
    }
    public function render()
    {
        $orders = Order::where('status','ordered')->orderBy('created_at','DESC')->paginate(12);
        return view('livewire.admin.admin-order-component',['orders'=>$orders])->layout('layouts.base');
    }
}
