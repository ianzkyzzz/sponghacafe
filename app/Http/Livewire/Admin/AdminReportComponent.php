<?php

namespace App\Http\Livewire\Admin;
use Alert;
use Livewire\Component;

class AdminReportComponent extends Component
{
    public $from;
    public $to;
    public function generate()
    {
        if($this->from && $this->to)
        {
            return redirect('/fpdf/sales.php/?from=' . $this->from . '&to=' . $this->to);
        }
        else
        {
           
        }
    }
    public function render()
    {
        return view('livewire.admin.admin-report-component')->layout('layouts.base');
    }
}
