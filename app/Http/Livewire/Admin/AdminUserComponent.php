<?php

namespace App\Http\Livewire\Admin;
use App\models\User;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
class AdminUserComponent extends Component
{
    use WithPagination;

    public function makeuserCashier($id)
    {
        if(Auth::user()->isAdmin == 0)
        {
            return redirect()->route('admin.orders');
            
        }
        else
        {
            $user = User::find($id);
            $user->utype = 'ADM';
            $user->isAdmin = 0;
            $user->save();
            session()->flash('message', 'User is now a Cashier');
        }
 
    }
    public function makeuserCustomer($id)
    {
        if(Auth::user()->isAdmin == 0)
        {
            return redirect()->route('admin.orders');
            
        }
        else{
            $user = User::find($id);
            $user->utype = 'USR';
            $user->isAdmin = 0;
            $user->save();
            session()->flash('message', 'User is now a Customer');
        }
 
    }
    public function render()
    {
        if(Auth::user()->isAdmin == 0)
        {
            return redirect()->route('admin.orders');
            
        }
        else
        {
            $users = User::where('id','!=', Auth::user()->id)->paginate(10);
            return view('livewire.admin.admin-user-component',['users'=>$users])->layout('layouts.base');
        }
       
    }
}
