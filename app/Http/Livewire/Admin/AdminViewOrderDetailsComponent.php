<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\models\Order;
use App\models\OrderItem;
use App\models\User;
class AdminViewOrderDetailsComponent extends Component
{
    public function mount($order_id)
    {
        $this->order_id = $order_id;
    }
    public function render()
    {
        $orders = Order::where('id', $this->order_id)->get();
     
        $orderitems = OrderItem::where('order_id', $this->order_id)->get();
        return view('livewire.admin.admin-view-order-details-component',['orders'=>$this->order_id,'orderitems'=>$orderitems])->layout('layouts.base');
    }
}
