<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Cart;
use App\models\Coupon;
use Illuminate\Support\Facades\Auth;

class CartComponent extends Component
{
    public $haveCouponCode;
    public $couponcode;
    public function increaseQuantity($rowId)
    {
        $menu = Cart::get($rowId);
        $qty = $menu->qty + 1;
        Cart::update($rowId,$qty);
    }
    public function decreaseQuantity($rowId)
    {
        $menu = Cart::get($rowId);
        $qty = $menu->qty - 1;
        Cart::update($rowId,$qty);
    }
    public function destroy($rowId)
    {
        Cart::remove($rowId);
        session()->flash('success_message', 'Item has been removed');
    }
    public function destroyAll()
    {
        Cart::destroy();
        session()->flash('success_message', 'Cart is now Empty');
    }
    public function checkout()
    {
        if(Auth::check())
        {
            
            return redirect()->route('menu.checkout');
        }
        else
        {
            return redirect()->route('login');
        }
    }
    public function setAmountForCheckout()
    {
        session()->put('checkout',[
            'discount' => 0,
            'subtotal' =>Cart::subtotal(),
            'tax' => 0,
            'total' => Cart::total()
        ]);
    }
    public function applyCouponCode()
    {
        $coupon = Coupon::where('code',$this->couponcode)->where('cart_value', '<=', Cart::instance('cart')->subtotal())->first();
        if(!$coupon)
        {
            session()->flash('coupon_message','Coupon code is invalid!!');
            return;
        }
        session()->put('coupon',[
            'code' =>$coupon->code,
            'type' =>$coupon->type,
            'value' =>$coupon->value,
            'cart_value' =>$coupon->cart_value
        ]);
    }
    public function render()
    {
        $this->setAmountForCheckout();
        return view('livewire.cart-component')->layout("layouts.base");
    }
}
