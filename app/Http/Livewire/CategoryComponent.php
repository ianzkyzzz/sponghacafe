<?php

namespace App\Http\Livewire;

use App\models\Menu;
use App\models\Category;
use Livewire\Component;
use Livewire\WithPagination;
use Cart;

class CategoryComponent extends Component
{
    public $sorting;
    public $pagesize;
    public $categoty_slug;

    public function mount($category_slug)
    {
        $this->sorting = "default";
        $this->pagesize = 12;
        $this->category_slug = $category_slug;
    }
    public function store($menu_id, $menu_name, $menu_price)
    {
        Cart::add($menu_id, $menu_name,1,$menu_price)->associate('App\Models\Menu');
        session()->flash('success_message', 'Item added in the Cart');
        return redirect()->route('menu.cart');
    }
    use WithPagination;
    public function render()
    {
        $category = Category::where('slug',$this->category_slug)->first();
        $category_id = $category->id;
        $category_name = $category->name;
        if($this->sorting =='date')
        {
            $menus = Menu::where('category_id', $category_id)->orderBy('created_at', 'DESC')->paginate($this->pagesize);
        }
        else if($this->sorting =='price')
        {
            $menus = Menu::where('category_id', $category_id)->orderBy('regular_price', 'ASC')->paginate($this->pagesize);
        }
        else if($this->sorting =='price-desc')
        {
            $menus = Menu::where('category_id', $category_id)->orderBy('regular_price', 'DESC')->paginate($this->pagesize);
        }
        else
        {
            $menus = Menu::where('category_id', $category_id)->paginate($this->pagesize);
        }
        $categories = Category::all();
        return view('livewire.category-component',['menus'=>$menus,'categories'=>$categories,'category_name'=>$category_name])->layout("layouts.base");
    }
}
