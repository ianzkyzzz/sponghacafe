<?php

namespace App\Http\Livewire;
use App\Mail\OrderMail;
use Livewire\Component;
use App\models\table;
use App\models\OrderItem;
use App\models\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Livewire\WithFileUploads;
use Carbon\Carbon;
use Cart;
use Alert;

class CheckoutComponent extends Component
{
    use WithFileUploads;
    public $paymentMethod; 
    public $image; 
    public $tbl;
    public $ordertype;
    public $thankyou;
    public $downpayment;
    public $forlater;

    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'paymentMethod' => 'required',
            'ordertype' => 'required'
        ]);
    }
    public function updateTbl()
    {
        if($this->tbl)
        {
            $tables = table::find($this->tbl);
            $tables->status ='1';
            $tables->save();
        }
       
       
    }
    public function placeOrder()
    {
        $this->validate([
            'paymentMethod' => 'required',
            'ordertype' => 'required'
        ]);
        if($this->ordertype=='pickup' && $this->tbl)
        {
            Alert::success('Congrats', 'malidsfsddf uyy');
        }
        else
        {
            $this->balance = session()->get('checkout')['subtotal'] - $this->downpayment;
    
            $order = new Order();
            $order->user_id = Auth::user()->id;
            $order->subtotal = session()->get('checkout')['subtotal'];
            $order->total = session()->get('checkout')['total'];
            $order->paymentMethod = $this->paymentMethod;
            if($this->image) {
                $imageName = Carbon::now()->timestamp. '.' . $this->image->extension();
                $this->image->storeAs('gcash',$imageName);
                $order->image = $imageName;
            }      
            $order->forlater = $this->forlater;
            $order->downpayment =$this->downpayment;
            $order->balance =$this->balance;
            if($this->tbl)
            {
                $order->tbl =$this->tbl;
            }
            else
            {
                $order->tbl = 99;
            }
            
            $order->ordertype = $this->ordertype;
            $order->status = 'ordered';
            $order->save();
    
            foreach(Cart::content() as $item)
            {
                $orderitem = new OrderItem();
                $orderitem->menu_id = $item->id;
                $orderitem->order_id = $order->id;
                $orderitem->price = $item->price;
                $orderitem->quantity = $item->qty;
                $orderitem->save();
                
    
            }
            $this->thankyou =1;
            Cart::destroy();
            session()->forget('checkout');
            if($order->tbl)
            { 
                $this->updateTbl();
            }
            $this->sendOrderConfirmation($order);
            return redirect()->route('thankyou');
        }
        
    }
    public function sendOrderConfirmation($order)
    {
        Mail::to($order->user->email)->send(new OrderMail($order));
    }
    public function verifyForCheckout()
    {
        if(!Cart::count() >0 )
        {
            session()->forget('checkout');
            return redirect()->route('menu.cart');
     
        }
        if(!Auth::check())
        {
            return redirect()->route('login');
        }
      
          
        
        else if(!session()->get('checkout'))
        {
            return redirect()->route('menu.cart');
        }
    }
    public function render()
    {
       
        $this->verifyForCheckout();
        $tables = table::where('status','0')->get();
        return view('livewire.checkout-component',['tables'=>$tables])->layout("layouts.base");
    }
}
