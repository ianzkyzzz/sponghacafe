<?php

namespace App\Http\Livewire;
use App\Models\Menu;
use Livewire\Component;
use Cart;

class DetailsComponent extends Component
{
    public $slug;
    
    public function mount($slug)
    {
        $this->slug = $slug;
    }
    public function store($menu_id, $menu_name, $menu_price)
    {
        Cart::add($menu_id, $menu_name,1,$menu_price)->associate('App\Models\Menu');
        session()->flash('success_message', 'Item added in the Cart');
        return redirect()->route('menu.shop');
    }
    public function render()
    {
        $menu = Menu::where('slug', $this->slug)->first();
        $popular_menu = Menu::inRandomOrder()->limit(4)->get();
        $related_menu = Menu::where('category_id',$menu->category_id)->inRandomOrder()->limit(5)->get();
        return view('livewire.details-component',['menu'=>$menu, 'popular_menu' => $popular_menu, 'related_menu' => $related_menu ])->layout('layouts.base');
    }
}
