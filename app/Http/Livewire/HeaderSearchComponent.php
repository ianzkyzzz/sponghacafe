<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\models\Category;
use App\models\Menu;
class HeaderSearchComponent extends Component
{
    public $search; 
    public $menu_cat;
    public $menu_cat_id;

    public function mount()
    {
        $this->menu_cat = 'All Category';
        $this->fill(request()->only('seach', 'menu_cat', 'menu_cat_id'));
    }
    public function render()
    {
        $categories = Category::all();
        return view('livewire.header-search-component',['categories'=>$categories]);
    }
}
