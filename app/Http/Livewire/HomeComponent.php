<?php

namespace App\Http\Livewire;
use App\models\HomeSlider;
use App\models\Menu;
use App\models\Order;
use Livewire\Component;

class HomeComponent extends Component
{
    public function render()
    {
        $popular_menus = Menu::inRandomOrder()->limit(10)->get();
       
        $latestmenus = Menu::withCount('ordermenu')->orderBy("ordermenu_count", 'desc')->limit(10)->get();
        $sliders = HomeSlider::where('status',1)->get();
        return view('livewire.home-component',['sliders'=>$sliders, 'latestmenus'=>$latestmenus,'popular_menus'=>$popular_menus])->layout('layouts.base');
    }
}
