<?php

namespace App\Http\Livewire;

use App\models\Menu;
use App\models\Category;
use Livewire\Component;
use Livewire\WithPagination;
use Cart;

class SearchComponent extends Component
{
    public $sorting;
    public $pagesize;

    public $search;
    public $menu_cat;
    public $menu_cat_id;
    public function mount()
    {
        $this->sorting = "default";
        $this->pagesize = 12;
        $this->fill(request()->only('search','menu_cat','menu_cat_id'));
    }
    public function store($menu_id, $menu_name, $menu_price)
    {
        Cart::add($menu_id, $menu_name,1,$menu_price)->associate('App\Models\Menu');
        session()->flash('success_message', 'Item added in the Cart');
        return redirect()->route('menu.cart');
    }
    use WithPagination;
    public function render()
    {
        if($this->sorting =='date')
        {
            $menus = Menu::where('name', 'like', '%'.$this->search . '%' )->where('category_id', 'like', '%'.$this->menu_cat_id.'%')->orderBy('created_at', 'DESC')->paginate($this->pagesize);
        }
        else if($this->sorting =='price')
        {
            $menus = Menu::where('name', 'like', '%'.$this->search . '%' )->where('category_id', 'like', '%'.$this->menu_cat_id.'%')->orderBy('regular_price', 'ASC')->paginate($this->pagesize);
        }
        else if($this->sorting =='price-desc')
        {
            $menus = Menu::where('name', 'like', '%'.$this->search . '%' )->where('category_id', 'like', '%'.$this->menu_cat_id.'%')->orderBy('regular_price', 'DESC')->paginate($this->pagesize);
        }
        else
        {
            $menus = Menu::where('name', 'like', '%'.$this->search . '%' )->where('category_id', 'like', '%'.$this->menu_cat_id.'%')->paginate($this->pagesize);
        }
        $categories = Category::all();
        return view('livewire.search-component',['menus'=>$menus,'categories'=>$categories])->layout("layouts.base");
    }
}
