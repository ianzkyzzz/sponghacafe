<?php

namespace App\Http\Livewire;

use App\models\Menu;
use App\models\Category;
use Livewire\Component;
use Livewire\WithPagination;
use Cart;

class ShopComponent extends Component
{
    public $sorting;
    public $pagesize;
    public function mount()
    {
        $this->sorting = "default";
        $this->pagesize = 12;
    }
    public function store($menu_id, $menu_name, $menu_price)
    {
        Cart::add($menu_id, $menu_name,1,$menu_price)->associate('App\Models\Menu');
        session()->flash('success_message', 'Item added in the Cart');
        return redirect()->route('menu.shop');
    }
    use WithPagination;
    public function render()
    {
        if($this->sorting =='date')
        {
            $menus = Menu::orderBy('created_at', 'DESC')->paginate($this->pagesize);
        }
        else if($this->sorting =='price')
        {
            $menus = Menu::orderBy('regular_price', 'ASC')->paginate($this->pagesize);
        }
        else if($this->sorting =='price-desc')
        {
            $menus = Menu::orderBy('regular_price', 'DESC')->paginate($this->pagesize);
        }
        else
        {
            $menus = Menu::paginate($this->pagesize);
        }
        $categories = Category::all();
        $popular_menus = Menu::inRandomOrder()->limit(10)->get();
        return view('livewire.shop-component',['menus'=>$menus,'categories'=>$categories,'popular_menus'=>$popular_menus])->layout("layouts.base");
    }
}
