<?php

namespace App\Http\Livewire\User;
use App\models\Order;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class UserDashboardComponent extends Component
{

    public function render()
    {
        $orders = Order::where('user_id','=', Auth::user()->id)->where('status','ordered')->paginate(10);
        $orders2 = Order::where('user_id','=', Auth::user()->id)->where('status','!=','ordered')->paginate(10);
        // dd($orders);


        return view('livewire.user.user-dashboard-component', ['orders'=> $orders,'orders2'=> $orders2])->layout("layouts.base");
    }
}
