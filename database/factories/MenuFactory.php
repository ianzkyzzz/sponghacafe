<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MenuFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $menu_name = $this->faker->unique()->words($nb=4,$asText=true);
        $slug = Str::slug($menu_name);
        return [
            'name' =>$menu_name,
            'slug' =>$slug,
            'short_description' => $this->faker->text(200),
            'description' => $this->faker->text(500),
            'regular_price' => $this->faker->numberBetween(30,300),
            'SKU'=> 'DIGI'.$this->faker->unique()->numberBetween(100,500),
            'stock_status' => 'instock',
            'quantity' => $this->faker->numberBetween(100,200),
            'image' => 'digital_' . $this->faker->unique()->numberBetween(1,22) . '.jpg', 
            'category_id' =>$this->faker->numberBetween(1,5)

        ];
    }
}
