<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->decimal('subtotal');
            $table->decimal('total');
            $table->decimal('discount')->default(0);
            $table->decimal('tax')->default(0);
            $table->decimal('downpayment');
            $table->decimal('balance');
            $table->dateTime('forLater');
            $table->enum('paymentMethod',['cash', 'gcash']);
            $table->enum('status',['ordered', 'cancelled', 'done']);
            $table->enum('ordertype',['pickup', 'dinein', 'deliver']);
            $table->bigInteger('tbl');
            $table->string('image')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
