<?php

require('../fpdf/cellfit.php');


class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');
    $this->Ln(20);


$this->Image('logo.jpg',87,8,-550);
    $this->Image('courtland.png',151.5,8,-720);
    $this->setFont("Arial",'B',12);
    $this->Cell(100.5,3,"Courtland Realty.",0,0,"C");
    $this->Cell(100.5,3,"Courtland Realty.",0,1,"C");

     $this->setFont("Arial",'',8);
    $this->Cell(100.5,3,"Purok Puso, Cabaluna Street, Barangay Gredu,",0,0,"C");
    $this->Cell(100.5,3,"Purok Puso, Cabaluna Street, Barangay Gredu,",0,1,"C");
    $this->Cell(100.5,3,"Panabo City, 8015, Davao del Norte, Philippines 8000",0,0,"C");
    $this->Cell(100.5,3,"Panabo City, 8015, Davao del Norte, Philippines 8000",0,1,"C");
    $this->Cell(100.5,3,"Tel no. (084) 309 1947",0,0,"C");
    $this->Cell(100.5,3,"Tel no. (084) 309 1947",0,1,"C");

    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(4);
}

// Page fo$oter

}

// Instanciation of inherited class


$pdf = new FPDF_CellFit();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
$pdf->Image('logo.jpg',30,5,-800);
$pdf->Image('logo.jpg',130,5,-800);
$pdf->Ln(20);
$pdf->setFont("Arial",'B',12);
$pdf->Cell(100.5,3,"R and Sons Properties Co.",0,0,"C");
$pdf->Cell(100.5,3,"R and Sons Properties Co.",0,1,"C");

 $pdf->setFont("Arial",'',8);
$pdf->Cell(100.5,3," 2nd floor S10 at The Paddock, J.Camus St.",0,0,"C");
$pdf->Cell(100.5,3," 2nd floor S10 at The Paddock, J.Camus St.",0,1,"C");
$pdf->Cell(100.5,3,"Corner General Luna St. , Brgy. 4-A Poblacion District,",0,0,"C");
$pdf->Cell(100.5,3,"Corner General Luna St. , Brgy. 4-A Poblacion District,",0,1,"C");
$pdf->Cell(100.5,3,"Davao City, Davao del Sur, Philippines 8000",0,0,"C");
$pdf->Cell(100.5,3,"Davao City, Davao del Sur, Philippines 8000",0,1,"C");
$pdf->Cell(100.5,3,"Tel no. (082) 322-6819",0,0,"C");
$pdf->Cell(100.5,3,"Tel no. (082) 322-6819",0,1,"C");

// $this->Cell(260,5,$_GET["cid"],0,1,"C");
// Line break
$pdf->Ln(4);
         $total = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'dbuser';
         $dbpass = '*/S3hd%/~]m~X<Zf';//*/S3hd%/~]m~X<Zf for testing live
         $dbname = 'randsons';//taging_dsr for testing live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }



         $sql = "SELECT CONCAT(AgentFname, ' ', AgentLname) AS agent FROM agents where agent_id =  '".$_GET['save']."'";
            $result = mysqli_query($conn, $sql);


           if ($result->num_rows == 1) {
             $row = $result->fetch_assoc();



                     $pdf->SetFont('Times','B',10);
                     $pdf->SetTextColor(194,8,8);
                     $pdf->Cell(85.5,5,'Agent`s Copy' ,0,0,'R');
                     $pdf->Cell(103.5,5,'RNS Copy'  ,0,1,'R');
                     $pdf->SetTextColor(0,0,0);

                     $pdf->Cell(103.5,5,'Agent Name : ' . iconv('UTF-8', 'windows-1252', $row["agent"]) ,0,0,'L');
                     $pdf->Cell(100.5,5,'Agent Name : ' . iconv('UTF-8', 'windows-1252', $row["agent"]) ,0,1,'L');
                     $pdf->Cell(103.5,5,'Date : ' . date("F j, Y", strtotime($_GET['date'])) ,0,0,'L');
                     $pdf->Cell(100.5,5,'Date : ' .  date("F j, Y", strtotime($_GET['date'])) ,0,1,'L');

}
$sql1 = "SELECT CONCAT(cl.`firstName`,' ', cl.`lastName`) AS clientName, CONCAT(pop.`propertyName`,' ', 'block ', pl.`block`, ' lot ', pl.`lot`) AS property,
comm.`cmid`, comm.`amount`,comm.`created_at`,comm.`comDetails` FROM commissions comm, propertylists pl, properties pop, clients cl, client__properties cp, agents ag
WHERE comm.`cp_id`=cp.`cp_id` AND cp.`client_id`=cl.`client_id` AND ag.`agent_id`=comm.`agent_id` AND cp.`propertylistid`=pl.`propertylistid`AND pl.`propId`=pop.`propId`
AND comm.`agent_id`='".$_GET['save']."' AND comm.`releaseDate` = '".$_GET['date']."' " ;
   $result1 = mysqli_query($conn, $sql1);

   $pdf->Cell(35,5,"Client Name",1,0,"C");
   $pdf->CellFitScale(32,5,"Property",1,0,"C");
   $pdf->CellFitScale(10,5,"#",1,0,"C");
   $pdf->CellFitScale(15.5,5,"amount",1,0,"C");
   $pdf->Cell(10,5,"",0,0,"C");
   $pdf->Cell(35,5,"Client Name",1,0,"C");
   $pdf->CellFitScale(32,5,"Property",1,0,"C");
   $pdf->CellFitScale(10,5,"#",1,0,"C");
   $pdf->CellFitScale(15.5,5,"amount",1,1,"C");
   $pdf->SetFont('Times','',10);
   if (mysqli_num_rows($result1) > 0) {
              while($rowz = mysqli_fetch_assoc($result1)) {



$pdf->Ln(0);

$pdf->CellFitScale(35,5,iconv('UTF-8', 'windows-1252', $rowz["clientName"]) . "(" . ($rowz["created_at"]).")",0,0,"C");
$pdf->CellFitScale(32,5,$rowz["property"],0,0,"C");
$pdf->CellFitScale(10,5,$rowz["comDetails"],0,0,"C");
$pdf->Cell(15.5,5,number_format(($rowz["amount"]),2,'.',',') ,0,0,"R");
$pdf->Cell(10,5,"",0,0,"C");
$pdf->CellFitScale(35,5,$rowz["clientName"] . "(" . ($rowz["created_at"]).")",0,0,"C");
$pdf->CellFitScale(32,5,$rowz["property"],0,0,"C");
$pdf->CellFitScale(10,5,$rowz["comDetails"],0,0,"C");
$pdf->Cell(15.5,5,number_format(($rowz["amount"]),2,'.',',') ,0,1,"R");
$total = $total + $rowz["amount"];



      $tax = $total * 0.05;
      $totalaftertax = $total - $tax;

}
}
$pdf->Ln(4);

$pdf->Cell(91.5,5,'Gross Commission :  '.number_format(( $total),2,'.',','),0,0,'R');
$pdf->Cell(103,5,'Gross Commission  :   '.number_format(($total),2,'.',','),0,1,'R');
$pdf->Ln(2);
$pdf->SetTextColor(194,8,8);
$pdf->Cell(91.5,5,'Less 5%(Tax) :  '.number_format(($tax),2,'.',','),0,0,'R');
$pdf->Cell(103,5,'Less 5%(Tax)  :   '.number_format(($tax),2,'.',','),0,1,'R');
$pdf->Ln(2);
$pdf->SetTextColor(0,0,0);
 $pdf->SetFont('Times','B',10);
$pdf->Cell(91.5,5,'Net Commission :  '.number_format(($totalaftertax),2,'.',','),0,0,'R');
$pdf->Cell(103,5,'Net Commission  :   '.number_format(($totalaftertax),2,'.',','),0,1,'R');










  $pdf->Ln(10);

  $pdf->Cell(103.5,5,'Released by : '.$_GET['user'],0,0,'L');
  $pdf->Cell(103.5,5,'Released by :  '.$_GET['user'],0,1,'L');
  $pdf->Ln(10);
  $pdf->Cell(100,5,'____________________' . '                 ____________________',0,0,'C');
    $pdf->Cell(100,5,'____________________' . '                 ____________________',0,1,'C');

$pdf->Cell(100,5,'            Agent`s Signature ' . '                                    Date' ,0,0,'L');

$pdf->Cell(100,5,'            Agent`s Signature ' . '                                    Date',0,0,'L');
         mysqli_close($conn);

$pdf->Output();
?>
