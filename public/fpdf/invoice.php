<?php
include_once("../fpdf/fpdf.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');

  $this->Image('logo.jpg',15,10,-1000);
  //$this->Image('logo.jpg',97,2,-1000);

    $this->setFont("Arial",'B',12);
    $this->Cell(198,5,"Spongha Cafe.",0,1,"C");
     $this->setFont("Arial",'',12);
    $this->Cell(198,5,"2nd Floor, Spongha Building",0,1,"C");
    $this->Cell(198,5,"USM Avenue, Kabacan, North Cotabato",0,1,"C");
    $this->Cell(198,5,"Tel no. (082) 235 1146",0,1,"C");
     // $this->setFont("Arial","I",10);
    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(26);
}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',8);
    // Page number
    $this->Cell(0,5,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,5,'Spongha Cafe',0,1,'C');
    $this->Cell(0,-5,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
         $total = 0;
         $balance = 0;
         $downpayment =0;
         
         $dbhost = 'localhost';
         $dbuser = 'root';
         $dbpass = '';//*/S3hd%/~]m~X<Zf for testing live
         $dbname = 'spongha';//tech_staging_dsr for testing live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         $sql1 = "SELECT usr.name as clName, odr.id, odr.ordertype,tbl.name as tblName,odr.total,odr.downpayment,odr.balance ,odr.created_at,odr.forLater
         FROM orders odr, users usr,TABLES tbl WHERE odr.user_id = usr.id AND odr.tbl = tbl.id and odr.id = '".$_GET['order']."' ";
            $resultz = mysqli_query($conn, $sql1);
         if ($resultz->num_rows == 1) {

$rowz = $resultz->fetch_assoc();
$total = $rowz["total"];
$balance =$rowz["balance"];
$downpayment =$rowz["downpayment"];
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"CLIENTS NAME: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["clName"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"ORDER NO.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["id"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"TABLE NO.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["tblName"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"ORDER TYPE.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["ordertype"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"DATE ORDERED.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,date("F  j, Y",strtotime($rowz["created_at"])),0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"ADVANCE BOOKING FOR: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,date("F  j, Y",strtotime($rowz["forLater"])+2*60*60),0,1,"L");



$pdf->SetFont('Times','B',10);
$pdf->Ln(8);

        }
        $pdf->SetFont('Times','B',10);
$pdf->Cell(5,5,"#",1,0,"C");
$pdf->Cell(80,5,"Ordered Food #",1,0,"C");
$pdf->Cell(30,5,"Price",1,0,"C");
$pdf->Cell(30,5,"Number of Serving",1,0,"C");
$pdf->Cell(30,5,"Total",1,1,"C");
// $pdf->Cell(30,5,"Bank",1,0,"C");
// $pdf->Cell(26,5,"Balance",1,1,"C");
    $pdf->SetFont('Times','',10);

         $sql = "SELECT mn.name, itm.price, itm.quantity, (itm.price * itm.quantity) AS total FROM order_items itm, menus mn,orders ors WHERE itm.menu_id = mn.id AND itm.order_id = '".$_GET['order']."' GROUP BY itm.id  ";
            $result = mysqli_query($conn, $sql);
            $n=0;
        
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
              
               




                $n++;
                $pdf->Cell(5,5,$n,0,0,"C");
                //


                //
                $pdf->Cell(80,5,$row["name"],0,0,"C");
                $pdf->Cell(30,5,number_format(($row["price"]),2,'.',','),0,0,"C");
                $pdf->Cell(30,5,$row["quantity"],0,0,"C");
            
                $pdf->Cell(30,5,number_format(($row["total"]),2,'.',','),0,0,"C");
         
            


                $pdf->Cell(30,5,"",0,1,"C");


                 
                }
                //  $pdf->Cell(5,5,$n,0,0,"C");
                // $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                // if ($row["particulars"]=="Downpayment"||($down==0&&$n==1)) {
                // $pdf->Cell(45,5,"Downpayment",0,0,"C");
                // }
                // else
                // {
                // $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                // }

                // $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(26,5,'',0,1,"C");



         } else {
             $pdf->Cell(196,5,"NO PAYMENTS YET!!",1,1,"C");
         }
           $pdf->Ln(8);
           $pdf->SetFont('Times','B',10);
         $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(80,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(30,5,"Subtotal",0,0,"C");
    $pdf->Cell(30,5,number_format(round($total,2),2,'.',','),0,1,"C");
    $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(80,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(30,5,"Paid Amount",0,0,"C");
    $pdf->Cell(30,5,number_format(round($downpayment,2),2,'.',','),0,1,"C");
    $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(80,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(30,5,"Balance",0,0,"C");
    $pdf->Cell(30,5,number_format(round($balance,2),2,'.',','),0,1,"C");


      $pdf->Cell(105,5,"",0,0,"L");
    
    $pdf->Cell(60,5,"",0,0,"C");
    $pdf->Cell(26,5,"" ,0,1,"C");

         mysqli_close($conn);

        //  $pdf->Output('', $name . "  " .$property . "  Block:" . $block. ", Lot:". $lot . ".pdf", false);
        $pdf->Output();

?>
