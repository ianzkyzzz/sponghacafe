<main id="main" class="main-site">

<div class="container">

    <div class="wrap-breadcrumb">
        <ul>
            <li class="item-link"><a href="/" class="link">home</a></li>
            <li class="item-link"><span>About Us</span></li>
        </ul>
    </div>
</div>

<div class="container">
    <!-- <div class="main-content-area"> -->
        <div class="aboutus-info style-center">
            <b class="box-title">Interesting Facts</b>
            <p class="txt-content">
                Spongha was once the biggest internet cafe in Kabacan where most of dota touranaments where held. Since internet cafe is not a thing anymore, the owner converted it to a cafe that we now known as Spongha Cafe.
            </p>
        </div>

        <div class="row equal-container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="aboutus-box-score equal-elem ">
                    <b class="box-score-title">Tasty Food</b>
                    <span class="sub-title">Best in Town</span>
                    <p class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text ever since the 1500s...</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="aboutus-box-score equal-elem ">
                    <b class="box-score-title">Cool place to dine</b>
                    <span class="sub-title">Fully Airconditioned</span>
                    <p class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text ever since the 1500s...</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="aboutus-box-score equal-elem ">
                    <b class="box-score-title">Accomodating staff</b>
                    <span class="sub-title">Friendy Staff</span>
                    <p class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the dummy text ever since the 1500s...</p>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="aboutus-info style-small-left">
                    <b class="box-title">What we really do?</b>
                    <p class="txt-content">We provide good quality dining at a very reasonable price</p>
                </div>
                <div class="aboutus-info style-small-left">
                    <b class="box-title">History of the Company</b>
                    <p class="txt-content">The Spongha Cafe we knew today was once an Internet Cafe.</p>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="aboutus-info style-small-left">
                    <b class="box-title">Our Vision</b>
                    <p class="txt-content">To continue to provide genuine food and good quality food dining for everyone.</p>
                </div>
                <div class="aboutus-info style-small-left">
                    <b class="box-title">Dine with us</b>
                    <p class="txt-content">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock,</p>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="aboutus-info style-small-left">
                    <b class="box-title">Dine with Us!</b>
                    <div class="list-showups">
                        <label>
                            <input type="radio" class="hidden" name="showup" id="shoup1" value="shoup1" checked="checked">
                            <span class="check-box"></span>
                            <span class='function-name'>Support 24/7</span>
                            <span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry...</span>
                        </label>
                        <label>
                            <input type="radio" class="hidden" name="showup" id="shoup2" value="shoup2">
                            <span class="check-box"></span>
                            <span class='function-name'>Best Quanlity</span>
                            <span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry...</span>
                        </label>
                        <label>
                            <input type="radio" class="hidden" name="showup" id="shoup3" value="shoup3">
                            <span class="check-box"></span>
                            <span class='function-name'>Fastest Delivery</span>
                            <span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry...</span>
                        </label>
                        <label>
                            <input type="radio" class="hidden" name="showup" id="shoup4" value="shoup4">
                            <span class="check-box"></span>
                            <span class='function-name'>Customer Care</span>
                            <span class="desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry...</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>

    <!-- </div> -->
    

</div><!--end container-->

</main>