<div>
    <div class="container" style="padding: 30px 0;">
        <div class="row">
                <div class="col-md-12">
                        <div class="panel panel-default">
                                <div class="panel-heading">
                                     <div class="row">
                                            <div class="col-md-6">
                                                Edit Menu
                                            </div>
                                            <div class="col-md-6">
                                                <a href="{{route('admin.menus')}}" class="btn btn-success pull-right">All Menus</a>
                                            </div>
                                     </div>
                                </div>
                                <div class="panel-body">
                                @if(Session::has('message'))
                                <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                                 @endif
                                        <form class="form-horizontal" enctype="multipart/form-data" wire:submit.prevent="updateMenu">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Menu Name </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control input-md" placeholder="Menu Name" wire:model="name" wire:keyup="generateSlug" />
                                                        @error('name') <p class="text-danger"> {{$message}} </p> @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Menu Slug </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control input-md" placeholder="Menu Slug" wire:model="slug" />
                                                        @error('slug') <p class="text-danger"> {{$message}} </p> @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Short Description </label>
                                                    <div class="col-md-4">
                                                        <textarea class="form-control" placeholder="Short Description" wire:model="short_description"> </textarea>
                                                        @error('short_description') <p class="text-danger"> {{$message}} </p> @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Description </label>
                                                    <div class="col-md-4">
                                                        <textarea class="form-control" placeholder="Description" wire:model="description"> </textarea>
                                                        @error('description') <p class="text-danger"> {{$message}} </p> @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Regular Price </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control input-md" placeholder="Regular Price" wire:model="regular_price"  />
                                                        @error('regular_price') <p class="text-danger"> {{$message}} </p> @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Sale Price </label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control input-md" placeholder="Sale Price" wire:model="sale_price"/>
                                                        @error('sale_price') <p class="text-danger"> {{$message}} </p> @enderror
                                                    </div>
                                                </div>

                                         

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> STOCK </label>
                                                    <div class="col-md-4">
                                                     <select class="form-control" wire:model="stock_status">
                                                         <option value="instock">InStock</option>
                                                         <option value="outofstock">Out of Stock</option>
                                                    </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Featured </label>
                                                    <div class="col-md-4">
                                                     <select class="form-control" wire:model="featured">
                                                         <option value="0">NO</option>
                                                         <option value="1">Yes</option>
                                                    </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Menu Image </label>
                                                    <div class="col-md-4">
                                                        <input type="file" class="input-file" wire:model="newimage"/>
                                                        @if($newimage)
                                                        <img src="{{$newimage->temporaryUrl()}}" width="120px" />
                                                        @error('newimage') <p class="text-danger"> {{$message}} </p> @enderror
                                                        @else
                                                        <img src="{{asset('assets/images/products')}}/{{$image}}" width="120px" />
                                                        @endif
                                                        
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Category </label>
                                                    <div class="col-md-4">
                                                     <select class="form-control" wire:model="category_id">
                                                         <option value="">Select Category</option>
                                                         @foreach ($categories as $category)
                                                         <option value="{{$category->id}}">{{$category->name}}</option>
                                                         @endforeach
                                                    </select>
                                                    @error('category_id') <p class="text-danger"> {{$message}} </p> @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"></label>
                                                    <div class="col-md-4">
                                                     <button type="submit" class="btn btn-primary">Update</button>
                                                    </div>
                                                </div>
                                        </form>
                                </div>
                        </div>
                </div>
        </div>
    </div>
</div>
