<div>
<div class="container" style="padding: 30px 0;">
    <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                       Edit Access
                    </div>
                    
                </div>
            </div>
            <div class="panel-body">
                @if(Session::has('message'))
                    <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                @endif
                    <form class="form-horizontal" wire:submit.prevent="updateCategory">
                        <div class="form-group">
                            <label class="col-md-4 control-label"> Category Name </label>
                            <div class="col-md-4">
                                <input type="text" placeholder="Category Name" class="form-control input-md" wire:model="name" w/>
                                @error('name') <p class="text-danger"> {{$message}} </p> @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"> Category Slug </label>
                            <div class="col-md-4">
                            <select class="form-control" wire:model="access">
                                                        <option value="CUS">CUSTOMER</option>
                                                         <option value="ADM">ADMIN</option>
                                                         <option value="CAS">CASHIER</option>
                                                         <option value="CUS">CUSTOMER</option>
                                                    </select>
                                @error('access') <p class="text-danger"> {{$message}} </p> @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label"> Category Name </label>
                            <div class="col-md-4">
                               <button type="submit" class="btn btn-primary" >Update </button>
                            </div>
                        </div>
                    </form>

            </div>

    </div>
</div>
</div>
