<div>
    <div class="container" style="padding:30px 0;">
            <div class="row">
                    <div class="col-md-12">
                            <div class="panel panel-default">
                                     <div class="panel-heading">
                                        Manage Home Heading
                                     </div>
                                     <div class="panel-body">
                                        <form class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Choose Category </label> 
                                                    <div class="col-md-4">
                                                        <select class="sel_categories form-control" name="categories[]" mulitiple="multiple">
                                                            @foreach($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                        </form>

                                     </div>
                            </div>
                    </div>
            </div>
    </div>
</div>
