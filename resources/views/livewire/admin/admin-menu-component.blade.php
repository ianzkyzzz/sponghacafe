<div>
<style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
        </style>
   <div class="container" style="padding:30px 0;">
            <div class="row">
                    <div class="col-md-12">
                            <div class="panel panel-default">
                                    <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-md-6">
                                          All Menu
                                      </div>
                                      <div class="col-md-6">
                                          <a href="{{route('admin.addmenu')}}" class="btn btn-success pull-right">Add New</a>
                                      </div>
                                  </div>
                                    </div>
                                    <div class="panel-body">
                                    @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">
                                {{Session::get('message')}}
                            </div>
                            @endif
                                            <table class="table table-stripped">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Image</th>
                                                        <th>Name</th>
                                                        <th>Stock</th>
                                                        <th>Price</th>
                                                        <th>Category</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                     
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                @foreach($menus as $menu)
                                                <tr>
                                                    <td>{{$menu->id}}</td>
                                                    <td><img src="{{asset('assets/images/products')}}/{{$menu->image}}" width="60" /></td>
                                                    <td>{{$menu->name}}</td>
                                                    <td>{{$menu->stock_status}}</td>
                                                    <td>{{$menu->regular_price}}</td>
                                                    <td>{{$menu->category->name}}</td>
                                                    <td>{{$menu->created_at}}</td>
                                                    <td>
                                                        <a href="{{route('admin.editmenu',['menu_slug'=>$menu->slug])}}"><i class="fa fa-edit fa-2x"> </i></a>
                                                        <a href="#" wire:click.prevent="deleteMenu({{$menu->id}})"> <i class="fa fa-times fa-2x text-danger"> </i> </a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{$menus->links()}}
                                    </div>
                            </div>
                    </div>
            </div>
    </div>
</div>
