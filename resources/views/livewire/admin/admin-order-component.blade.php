<div>
<style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
        </style>
   <div class="container" style="padding:30px 0;">
            <div class="row">
                    <div class="col-md-12">
                            <div class="panel panel-default">
                                    <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-md-6">
                                          All Orders
                                      </div>
                                      
                                  </div>
                                    </div>
                                    <div class="panel-body">
                                    @if(Session::has('message'))
                            <div class="alert alert-success" role="alert">
                                {{Session::get('message')}}
                            </div>
                            @endif
                            @if(Session::has('message_cancel'))
                            <div class="alert alert-danger" role="alert">
                                {{Session::get('message_cancel')}}
                            </div>
                            @endif
                                            <table class="table table-stripped">
                                                <thead>
                                                    <tr>
                                                        <th>Order ID</th>
                                                        <th>image</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>OrderType</th>
                                                        <th>Table</th>
                                                        <th>Total</th>
                                                        <th>Date</th>
                                                        <th>Action</th>
                                                     
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                @foreach($orders as $order)
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                    <td>
                                                        @if($order->image)
                                                        <img src="{{asset('assets/images/gcash')}}/{{$order->image}}" width="60" />
                                                        @else
                                                        NO ATTACHED Payment
                                                        @endif
                                                    </td>
                                                    <td>{{$order->user->name}}</td>
                                                    <td>{{$order->user->email}}</td>
                                                    <td>{{$order->ordertype}}</td>
                                                    <td>
                                                        @if($order->tbl=='99')
                                                        TAKE OUT/RESERVE FOR LATER
                                                        @else
                                                    {{$order->tbl}}
                                                    @endif
                                                    </td>
                                                    <td>{{$order->balance}}</td>
                                                    <td>{{$order->forlater}}</td>
                                                    <td>
                                                    <a href="#" class="btn btn-primary btn-small" wire:click.prevent="finishOrder({{$order->id}},{{$order->tbl}})">Order Finished</a>
                                                    <a class="btn btn-success btn-small" href="{{route('admin.orderdetails',['order_id'=>$order->id])}}">Wiew Details</a>
                                                    <a class="btn btn-danger btn-small" href="#" wire:click.prevent="cancelOrder({{$order->id}})">Cancel</a>

                                                    </td>
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{$orders->links()}}
                                    </div>
                            </div>
                    </div>
            </div>
    </div>
</div>
