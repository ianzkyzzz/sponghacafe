<div>
    <div class="container" style="padding:30px 0;">
            <div class="row">
                    <div class="col-md-12">
                            <div class="panel panel-default">
                                     <div class="panel-heading">
                                        Generate Sales Report
                                     </div>
                                     <div class="panel-body">
                                        <form class="form-horizontal" wire:submit.prevent="generate">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Choose Start Date </label> 
                                                    <div class="col-md-4">
                                                    <input type="date" class="form-control input-md" placeholder="Sale Price" wire:model="from"/>
                                                    </div>
                                                </div>

                                                <form class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Choose Finish Date  </label> 
                                                    <div class="col-md-4">
                                                    <input type="date" class="form-control input-md" placeholder="Sale Price" wire:model="to"/>
                                                    </div>
                                                </div>
                                                <form class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"></label> 
                                                    <div class="col-md-4">
                                                    <button type="submit" class="btn btn-primary">GENERATE </button>
                                                    </div>
                                                </div>
                                             
                                        </form>
                                           
                                     </div>
                            </div>
                    </div>
            </div>
    </div>
</div>
