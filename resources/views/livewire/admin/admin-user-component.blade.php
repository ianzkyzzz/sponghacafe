<div>
    <style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
        </style>
    <div class="container" style="padding: 30px p;">
        <div class="row">
            <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6">
                                    All Users
                                </div>
                                
                            </div>
                        </div>
                        <div class="panel-body">

                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th> Name </th>
                                        <th>Email</th>
                                        <th> AccessType </th>
                                        <th> Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                            <td>{{$user->name}} </td>
                                            <td> {{$user->email}}</td>
                                            <td>
                                                @if($user->utype == 'ADM' && $user->isAdmin == 1)
                                                ADMIN
                                                @elseif($user->utype == 'ADM' && $user->isAdmin == 0)
                                                CASHIER
                                                @else
                                                CUSTOMER
                                                @endif
                                            
                                        
                                        </td>
                                            <td>
    

                                            <a href="#"  wire:click.prevent="makeuserCashier({{$user->id}})"> <i class="fa fa-user"> to cashier</i></a>  
                                            <a href="#"  wire:click.prevent="makeuserCustomer({{$user->id}})"> <i class="fa fa-edit"> to customer</i></a>  
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table> 
                            {{$users->links()}}   
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
