<div>
    <style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
        </style>
    <div class="container" style="padding: 30px p;">
        <div class="row">
            <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <h3>
                                <div class="col-md-6">
                                    ORDER NO. : 
                                   {{$orders}}
                                </div>
                                </h3>
                                
                            </div>
                        </div>
                        <div class="panel-body">
               
                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Order Name</th>
                                        <th> Order Price</th>
                                        <th> Order Quantity </th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orderitems as $item)
                                    <tr>
                                            <td><img src="{{asset('assets/images/products')}}/{{$item->menu->image}}" width="60" /></td>
                                            <td>{{$item->menu->name}} </td>
                                            <td> {{$item->price}}</td>
                                            <td>{{$item->quantity}} </td>
                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table> 
                               
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
