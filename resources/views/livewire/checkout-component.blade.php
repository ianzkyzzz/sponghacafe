	<main id="main" class="main-site">

		<div class="container">

			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="/" class="link">home</a></li>
					<li class="item-link"><span>checkout</span></li>
				</ul>
			</div>
			<div class=" main-content-area">
				<div class="wrap-address-billing" enctype="multipart/form-data">
					<h3 class="box-title">Order Info</h3>
					@if(Session::has('warning'))
					<div class="alert alert-warning">
						<strong> success </strong> {{Session::get('warning')}}
					</div>
					@endif
					<form   wire:submit.prevent="placeOrder" >
						<p class="row-in-form">
							<label for="fname">Available Tables<span>*</span></label>
						<select class = "form-control"  wire:model="tbl">
						<option value="99">Please Select Table</option>
						@foreach($tables as $table)
						<option value="{{$table->id}}">{{$table->name}}</option>
						@endforeach
						</select>
						</p>
						<p class="row-in-form">
							<label for="lname">Dine<span>*</span></label>
							<select class = "form-control"  wire:model="ordertype">
							<option value="">Select Order Type</option>
							<option value="dinein"> Dine in</option>
							<option value="pickup">Take Out</option>
							</select>
							@error('ordertype') <p class="text-danger"> {{$message}} </p> @enderror
						</p>
						<p class="row-in-form">
							<label for="lname">Payment/Downpayment<span>*</span></label>
							<input id="lname" type="text" name="downpayment" value="" placeholder="Downpayment" wire:model="downpayment">
							@error('downpayment') <p class="text-danger"> {{$message}} </p> @enderror
						</p>
						<p class="row-in-form">
							<label for="lname">Schedule<span>*</span></label>
							<input id="lname" type="datetime-local" name="forlater" class="form-control" value="" placeholder="Book for later" wire:model="forlater">
							@error('forlater') <p class="text-danger"> {{$message}} </p> @enderror
						</p>
					
						
				
						<div class="summary summary-checkout">
						<p  style="text-align:center;"><img src="{{asset('assets/images/products/scanme.jpg')}}" width="350" /></p>
					<div class="summary-item payment-method">
						<h4 class="title-box">Payment Method</h4>
						@error('paymentMethod') <p class="text-danger"> {{$message}} </p> @enderror
						<div class="choose-payment-methods">
							<label class="payment-method">
							<select class = "form-control"  wire:model="paymentMethod">
							<option value="">Select Payment Method</option>
							<option value="gcash">GCASH</option>
							<option value="cash">CASH</option>
							</select>
								<span class="payment-desc">Please Screen Shot your gcash payment and upload</span>
							</label>
							<input type="file" class="input-file" wire:model="image" wire:model="image">
						
						@if(Session::has('checkout'))
						<p class="summary-info grand-total"><span>Grand Total</span> <span class="grand-total-price">{{Session::get('checkout')['total']}}</span></p>
						@endif
					
						</form>
						<button type="submit" class="btn btn-medium">Place Order</button>
						
					</div>
					
				</div>

					
				</div>
				
			

			</div><!--end main content area-->
		</div><!--end container-->

	</main>
