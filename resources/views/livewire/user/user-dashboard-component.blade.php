<div>

<div>
    <style>
    
        </style>
    <div class="container" style="padding: 30px p;">
        <div class="row">
        <h1> Order History </h1>
            <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6">
                                   Active Orders
                                </div>
                                
                            </div>
                        </div>
                        <div class="panel-body">
                      
                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th> ORDER ID </th>
                                        <th> TOTAL</th>
                                        <th> DOWNPAYMENT </th>
                                        <th> BALANCE </th>
                                        <th>For Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                    <tr>
                                            <td>{{$order->id}} </td>
                                            <td> {{$order->total}}</td>
                                            <td>{{$order->downpayment}} </td>
                                            <td>{{$order->balance}} </td>
                                            <td>
                                                @if($order->forLater)
                                               {{$order->forLater}}
                                               @else
                                               {{$order->created_at}}
                                               @endif
                                              
                                             </td>
                                            <td>
                                            <a href="/fpdf/invoice.php/?order={{$order->id}}"><i class="fa fa-edit fa-2x"> </i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table> 
                           
                        </div>
                    </div>
            </div>
        </div>
    </div>

<br>
    <div class="container" style="padding: 30px p;">
        <div class="row">
      
            <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-md-6">
                                    Recent Orders
                                </div>
                                
                            </div>
                        </div>
                        <div class="panel-body">
                      
                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th> ORDER ID </th>
                                        <th> TOTAL</th>
                                        <th> DOWNPAYMENT </th>
                                        <th> BALANCE </th>
                                        <th>For Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders2 as $order2)
                                    <tr>
                                            <td>{{$order2->id}} </td>
                                            <td> {{$order2->total}}</td>
                                            <td>{{$order2->downpayment}} </td>
                                            <td>{{$order2->balance}} </td>
                                            <td>
                                                @if($order2->forLater)
                                               {{$order2->forLater}}
                                               @else
                                               {{$order2->created_at}}
                                               @endif
                                              
                                             </td>
                                            <td>
                                   
                                       
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                               
                            </table> 
                                
                        </div>
                        {{$orders2->links()}}
                    </div>
                    
            </div>
            
        </div>
    </div>
<br>

</div>

</div>
