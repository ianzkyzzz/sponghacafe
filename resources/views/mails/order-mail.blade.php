<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Order Confirmation</title>

<body>
	<p> Hi {{$order->user->name}}</p>
	<p> Your Order has been successfully placed.</p>
	<br/>

	<table style="width: 600px; text-align:right">
<thead>
	<tr>
		<td>Image</td>
			<td>Name </td>
				<td>Quantity</td>
					<td>Price</td>
	</tr>
</thead>
<tbody>
	@foreach($order->orderItems as $item)
	<tr>
			<td> <img src="{{asset('assets/images/products')}}/{{$item->menu->image}}" width="200" /></td>
            <td>{{$item->menu->name}}</td>
            <td>{{$item->quantity}}</td>
            <td>{{$item->price * $item->quantity}}</td>
	</tr>
    @endforeach
    <tr>
        <td colspan="3" style="border-top:1px solid #ccc;"></td>
        <td style="font-size:15px; font-weight:bold; border-top:1px solid #ccc;">Tendered Amount: Php {{$order->total}}</td>
    </tr>
    <tr>
		@if($order->downpayment)
        <td colspan="3" style="border-top:1px solid #ccc;"></td>
        <td style="font-size:15px; font-weight:bold; border-top:1px solid #ccc;">Paid Amount: Php {{$order->downpayment}}</td>
		@else
		<td colspan="3" style="border-top:1px solid #ccc;"></td>
        <td style="font-size:15px; font-weight:bold; border-top:1px solid #ccc;">Paid Amount: Php 0.00</td>
		@endif
    </tr>
    <tr>
        <td colspan="3" style="border-top:1px solid #ccc;"></td>
        <td style="font-size:15px; font-weight:bold; border-top:1px solid #ccc;">Balance to Pay: Php {{$order->balance}}</td>
    </tr>
</tbody>
	 </table>





</body>
</html>
