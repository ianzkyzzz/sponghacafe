<?php
use App\Http\Livewire\HomeComponent;
use App\Http\Livewire\ShopComponent;
use App\Http\Livewire\CartComponent;
use App\Http\Livewire\CheckoutComponent;
use App\Http\Livewire\DetailsComponent;
use App\Http\Livewire\CategoryComponent;
use App\Http\Livewire\SearchComponent;
use App\Http\Livewire\AboutUsComponent;
use App\Http\Livewire\ContactPageComponent;

use App\Http\Livewire\User\UserDashboardComponent;
use App\Http\Livewire\Admin\AdminDahboardComponent;
use App\Http\Livewire\Admin\AdminCategoryComponent;
use App\Http\Livewire\Admin\AdminAddCategoryComponent;
use App\Http\Livewire\Admin\AdminEditCategoryComponent;
use App\Http\Livewire\Admin\AdminMenuComponent;
use App\Http\Livewire\Admin\AdminAddMenuComponent;
use App\Http\Livewire\Admin\AdminEditMenuComponent;
use App\Http\Livewire\Admin\AdminHomeSliderComponent;
use App\Http\Livewire\Admin\AdminAddHomeSliderComponent;
use App\Http\Livewire\Admin\AdminEditHomeSliderComponent;
use App\Http\Livewire\Admin\AdminCouponsComponent;
use App\Http\Livewire\Admin\AdminAddCouponsComponent;
use App\Http\Livewire\Admin\AdminEditCouponsComponent;
use App\Http\Livewire\Admin\AdminOrderComponent;
use App\Http\Livewire\Admin\AdminUserComponent;
use App\Http\Livewire\Admin\AdminEditUserComponent;
use App\Http\Livewire\Admin\AdminReportComponent;
use App\Http\Livewire\Admin\AdminViewOrderDetailsComponent;

use App\Http\Livewire\ThankYouComponent;


use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', HomeComponent::class);
Route::get('/shop', ShopComponent::class)->name('menu.shop');
Route::get('/aboutus', AboutUsComponent::class)->name('menu.aboutus');
Route::get('/contactus', ContactPageComponent::class)->name('menu.contactus');
Route::get('/cart', CartComponent::class)->name('menu.cart');
Route::get('/checkout', CheckoutComponent::class)->name('menu.checkout');
Route::get('/menu/{slug}', DetailsComponent::class)->name('menu.details');
Route::get('/menu-category/{category_slug}', CategoryComponent::class)->name('menu.category');
Route::get('/search', SearchComponent::class)->name('menu.search');
Route::get('/thankyou', ThankYouComponent::class)->name('thankyou');
Route::get('/admin/users/edit/{user_id}', AdminEditUserComponent::class)->name('admin.editnathis');
// Route::middleware([
//     'auth:sanctum',
//     config('jetstream.auth_session'),
//     'verified'
// ])->group(function () {
//     Route::get('/dashboard', function () {
//         return view('dashboard');
//     })->name('dashboard');
// });

//For AdminUser
Route::middleware(['auth:sanctum','verified'])->group(function(){
    Route::get('/user/dashboard', UserDashboardComponent::class)->name('user.dashboard');
});


//For NormalUSer
Route::middleware(['auth:sanctum','verified','authadmin'])->group(function(){
    Route::get('/admin/dashboard', AdminDahboardComponent::class)->name('admin.dashboard');
    Route::get('/admin/categories', AdminCategoryComponent::class)->name('admin.categories');
    Route::get('/admin/category/add', AdminAddCategoryComponent::class)->name('admin.addcategory');
    Route::get('/admin/category/edit/{category_slug}', AdminEditCategoryComponent::class)->name('admin.editcategory');
    Route::get('/admin/menus', AdminMenuComponent::class)->name('admin.menus');
    Route::get('/admin/menu/add', AdminAddMenuComponent::class)->name('admin.addmenu');
    Route::get('/admin/menu/edit/{menu_slug}', AdminEditMenuComponent::class)->name('admin.editmenu');

    Route::get('/admin/slider', AdminHomeSliderComponent::class)->name('admin.homeslider');
    Route::get('/admin/slider/add', AdminAddHomeSliderComponent::class)->name('admin.addhomeslider');
    Route::get('/admin/slider/edit/{slide_id}', AdminEditHomeSliderComponent::class)->name('admin.edithomeslider');
    Route::get('/admin/coupons', AdminCouponsComponent::class)->name('admin.coupon');
    Route::get('/admin/coupons/add', AdminAddCouponsComponent::class)->name('admin.addcoupon');
    Route::get('/admin/coupons/{coupon_id}', AdminEditCouponsComponent::class)->name('admin.editcoupon');
    Route::get('/admin/orders', AdminOrderComponent::class)->name('admin.orders');
    Route::get('/admin/users', AdminUserComponent::class)->name('admin.users');
    Route::get('/admin/report', AdminReportComponent::class)->name('admin.report');
    Route::get('/admin/orders/details/{order_id}', AdminViewOrderDetailsComponent::class)->name('admin.orderdetails');
   


});

